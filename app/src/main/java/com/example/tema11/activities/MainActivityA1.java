package com.example.tema11.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.example.tema11.R;
import com.example.tema11.interfaces.ActivityFragmentCommunication;

public class MainActivityA1 extends AppCompatActivity implements ActivityFragmentCommunication {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_a1);
    }

    @Override
    public void openA2() {
        Intent intent = new Intent(this, MainActivityA2.class);
        startActivity(intent);
    }

    @Override
    public void replaceF2A2_F3A2() {

    }
}