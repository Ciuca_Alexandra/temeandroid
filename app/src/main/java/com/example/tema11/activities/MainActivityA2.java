package com.example.tema11.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

import com.example.tema11.R;
import com.example.tema11.fragments.F1A2;
import com.example.tema11.fragments.F3A2;
import com.example.tema11.interfaces.ActivityFragmentCommunication;

public class MainActivityA2 extends AppCompatActivity implements ActivityFragmentCommunication {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_a2);
        addF1A2();
    }

    public void addF1A2()
    {
        FragmentManager fragmentManager = getSupportFragmentManager();
        androidx.fragment.app.FragmentTransaction transaction = fragmentManager.beginTransaction();
        String tag = F1A2.class.getName();
        FragmentTransaction addTransaction = transaction.add(
                R.id.f1a2, F1A2.newInstance(), tag);

        addTransaction.commit();
    }

    @Override
    public void openA2() {

    }

    @Override
    public void replaceF2A2_F3A2()
    {
        FragmentManager fragmentManager = getSupportFragmentManager();
        androidx.fragment.app.FragmentTransaction transaction = fragmentManager.beginTransaction();
        String tag = F3A2.class.getName();
        FragmentTransaction replaceTransaction = transaction.replace(
                R.id.f3a2, F3A2.newInstance("", ""), tag);

        replaceTransaction.commit();
    }
}